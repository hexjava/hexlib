package hexlib.window;

import javax.swing.*;

/**
 * HexLib WindowManager class
 *
 * @author astronomize
 * @version 1.00
 */
public static class WindowManager {
    private JFrame frame = null;
    
    //creates a window
    public static void createWindow(String name) {
        frame = new JFrame(name);
    }
    
    //creates a window.
    public static void createWindow() {
        frame = new JFrame();
    }
    
    //sets the window's size to the specified size.
    public static void setWindowSize(int width, int height) {
        frame.setSize(width, height);
    }
    
    //sets the window's title to the specified title.
    public static void setWindowTitle(String title) {
        frame.setTitle(title);
    }
    
    //sets the window's visibility to the specified visibility.
    public static void setWindowOpen(boolean open) {
        frame.setVisible(open);
    }
}