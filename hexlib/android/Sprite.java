package hexlib.android;

import android.media.Image;

public class Sprite {
    private Image img;
    public Sprite(Image img) {
        this.img = img;
    }
    
    public Image getSprite() {
        return img;
    }
}